//
//  SceneDelegate.h
//  Pergolas
//
//  Created by Vy Ly on 11/7/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

