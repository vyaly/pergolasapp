//
//  PLPhotoItem.h
//  Pergolas
//
//  Created by Vy Ly on 11/8/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLPhotoItem : NSObject
@property(nonatomic, assign) NSInteger albumId;
@property(nonatomic, assign) NSInteger id;

@property(nonatomic, strong) NSURL *thumbnailURL;
@property(nonatomic, assign) NSString *title;
@property(nonatomic, strong) NSURL *url;

+(PLPhotoItem*)itemFromJsonObject:(NSDictionary *)object;
@end

NS_ASSUME_NONNULL_END
