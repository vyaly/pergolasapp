//
//  JsonDownloader.m
//  Pergolas
//
//  Created by Vy Ly on 11/8/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import "PLResourceManager.h"
#import <os/log.h>
#import "PLPhotoItem.h"
@interface PLResourceManager()
//@property NSData* dataBlob;
//@property NSMutableArray<PLPhotoItem*> *res;

@end

@implementation PLResourceManager

+ (instancetype)sharedInstance
{
    static PLResourceManager *sharedInstance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedInstance = [[PLResourceManager alloc] init];
    });
    return sharedInstance;
}

- (void)fetchPhotosWithCompletion:(void (^)(NSArray<PLPhotoItem*> *photos, BOOL success))completionHandler
{
    NSURL *url = [[NSURL alloc] initWithString:@"https://jsonplaceholder.typicode.com/photos"];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];

    NSURLSessionDataTask *data = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (error)
        {
            NSLog(@"Error fetching photos %@", error);
            completionHandler(nil,NO);
            return;
        }
        if (httpResponse.statusCode == 200)
        {
            completionHandler([self parsePhotoData:data], YES);
            return;
        }
        completionHandler(nil, NO);
    }];
    [data resume];
}

- (NSMutableArray<PLPhotoItem*>*)parsePhotoData:(NSData *)data
{
    NSError *error = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error)
    {
        NSLog(@"parsePhotoData error %@", error);
    }
    NSMutableArray<PLPhotoItem*> *res = [[NSMutableArray alloc] init];
    for (NSDictionary *object in jsonArray) {
        PLPhotoItem *item = [PLPhotoItem itemFromJsonObject:object];
        [res addObject:item];
    }
    return res;
}

-(void)downloadImageForURL:(NSURL*)url
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
//     __weak typeof(self) weakSelf = self;
    NSURLSessionDataTask *data = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//         __strong typeof(self) strongSelf = weakSelf;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        if (error)
        {
            NSLog(@"Error fetching photos %@", error);
        }
        if(httpResponse.statusCode == 200)
        {
            UIImage *imageData = [UIImage imageWithData:data];
        }
        
    }];
    [data resume];
    
}


@end
