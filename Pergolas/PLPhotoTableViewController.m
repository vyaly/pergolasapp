//
//  PhotoTableViewController.m
//  Pergolas
//
//  Created by Vy Ly on 11/8/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import "PLPhotoTableViewController.h"
#import "PLResourceManager.h"
#import "PLPhotoItem.h"

@interface PLPhotoTableViewController()

@property (strong, nonatomic) NSArray<PLPhotoItem*> *photoData;

@end

@implementation PLPhotoTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[PLResourceManager sharedInstance] fetchPhotosWithCompletion:^(NSArray<PLPhotoItem *> * _Nonnull photos, BOOL success) {
        if (success) {
            NSLog(@"PL: Got photos success");
            dispatch_async(dispatch_get_main_queue(), ^{
                self.photoData = photos;
                [self.tableView reloadData];
            });
        } else {
            NSLog(@"PLPhotoTableViewController error retrieving photos");
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.photoData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"jsonCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"jsonCell"];
        cell.textLabel.text = self.photoData[indexPath.row].title;
    }
    return cell;
}


@end
