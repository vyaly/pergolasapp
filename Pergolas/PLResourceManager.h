//
//  PLResourceManager.h
//  Pergolas
//
//  Created by Vy Ly on 11/8/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@class PLPhotoItem;

@interface PLResourceManager : NSObject
+ (instancetype)sharedInstance;
- (void)fetchPhotosWithCompletion:(void (^)(NSArray<PLPhotoItem*> *photos, BOOL success))completionHandler;
@end

NS_ASSUME_NONNULL_END
