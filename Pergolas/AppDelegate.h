//
//  AppDelegate.h
//  Pergolas
//
//  Created by Vy Ly on 11/7/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

