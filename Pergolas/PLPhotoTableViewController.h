//
//  PhotoTableViewController.h
//  DisplayJson
//
//  Created by Vy Ly on 11/8/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface PLPhotoTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
