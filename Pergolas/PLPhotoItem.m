//
//  PLPhotoItem.m
//  Pergolas
//
//  Created by Vy Ly on 11/8/19.
//  Copyright © 2019 Vy Ly. All rights reserved.
//

#import "PLPhotoItem.h"

@implementation PLPhotoItem

+ (PLPhotoItem*)itemFromJsonObject:(NSDictionary *)jsonObject
{
    PLPhotoItem *item = [[PLPhotoItem alloc] init];
    item.albumId = [[jsonObject objectForKey:@"albumId"] integerValue];
    id title = [jsonObject objectForKey:@"title"];
    if ([title isKindOfClass:[NSString class]]) {
        item.title = title;
    }
    return item;
}

@end
